/* eslint-disable guard-for-in*/

$(function() {
  var tcData = {
    main: {
      'hour': 15,
      'minute': 30,
      'duration': 30,
      'dayS': 'Monday',
      'dayE': 'Thursday',
      'originTz': 'America/Los_Angeles'
    },
    apac: {
      'hour': 14,
      'minute': parseInt('00', 10),
      'duration': 30,
      'dayS': 'Tuesday',
      'originTz': 'Australia/Sydney'
    }
  };

  function endDayHelper(timezone, tz, utcTime) {
    return {
      'PST': moment(timezone).tz('America/Los_Angeles').day(tcData.main.dayE).format('dddd'),
      'UTC': moment(timezone).day(tcData.main.dayE).format('dddd'),
      'USER': moment.tz(moment(utcTime).day(tcData.main.dayE), moment.tz.guess()).format('dddd')
    }[tz];
  }

  function setTimeDate(tc) {
    var now = moment().tz(tc.originTz);
    var utcTime = now.set({
      'day': tc.dayS,
      'hour': tc.hour,
      'minute': tc.minute,
      'second': 0
    });

    var userTime = moment.tz(utcTime, moment.tz.guess());
    var pstTime = moment.tz(utcTime, 'America/Los_Angeles');
    var timezones = {
      PST: pstTime,
      UTC: utcTime,
      USER: userTime
    };

    for (tz in timezones) {
      var cTZ = timezones[tz];

      var startT = cTZ.format('hh:mm a');
      var endT = cTZ.add(tc.duration, 'm').format('hh:mm a');
      var startD = cTZ.format('dddd');
      var endD = endDayHelper(cTZ, tz, utcTime);

      $('#' + timezone + '-' + 'abbr').html(cTZ.zoneAbbr());
      $('#' + timezone + '-' + tz).html(startT + ' to ' + endT + ' - ' + startD + (timezone === 'main' ? ' to ' + endD : ''));
    }
  }

  for (timezone in tcData) {
    setTimeDate(tcData[timezone]);
  }
});
