---
layout: markdown_page
title: "Chief of Staff Team"
description: "GitLab CoS Team Handbook"
---

## On this page
{:.no_toc}

- TOC
{:toc .toc-list-icons}

{::options parse_block_html="true" /}

----


## Quick Links and Fun Facts
* [Chief of Staff Job Family](/job-families/chief-executive-officer/chief-of-staff/)
* [Internal Strategy Consultant Job Family](/job-families/chief-executive-officer/internal-strategy-consultant/)
* CoS = Chief of Staff
* CoST = Chief of Staff Team


## Contact Us
* [chief-of-staff-team](https://gitlab.slack.com/archives/CN7MPDZF0/p1568035351000200) on Slack


